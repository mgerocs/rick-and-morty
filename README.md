# Rick and Morty - Code Challenge

### Install and Run

The project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
After cloning the project install the dependencies using **npm install** from the project directory.
After the dependencies have been installed run **npm start** and then open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Notes from the developer

- I used TypeScript.
- I used Function Components with Hooks.
- I implemented the List component in a way so that it is decoupled from the logic of fetching the data from the API.
- I split up the List component into smaller components each having a specific functionality (search, filter, pagination, etc.).
- I implemented preloading for the images.
- I implemented debouncing for the search.

### What else could be added if I had more time?

- I wrote a couple of unit tests, but obviously more would be nice.
- End-to-end testing would be nice.
- Pagination is not very nice - truncation with ellipsis (...) and arrows would be much better and took up less space.
- More filters could be added.
- Styling in general could be improved.





