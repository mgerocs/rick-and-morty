import axios, { AxiosResponse } from "axios";

import { CHARACTERS_URL } from "../config/API.config";

export interface IInfo {
  count: number;
  next?: string;
  pages: number;
  prev?: string;
}

export interface ICharacter {
  id: number;
  name: string;
  status: "Alive" | "Dead" | "unknown";
  species: string;
  type: string;
  gender: "Male" | "Female" | "Genderless" | "unknown";
  origin: { name: string; url: string };
  location: { name: string; url: string };
  image: string;
  episode: string[];
  url: string;
  created: string;
}

export interface IFilter {
  status: string;
}

export const getCharacters = (
  pageNo: number,
  name?: string,
  filter?: IFilter
): Promise<AxiosResponse> => {
  // concetanating query params
  const params = [
    `page=${pageNo}`,
    `name=${name || ""}`,
    `status=${filter?.status || ""}`,
  ].join("&");
  return axios.get<{ info: IInfo; results: ICharacter[] }>(
    CHARACTERS_URL + `?${params}`,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
};
