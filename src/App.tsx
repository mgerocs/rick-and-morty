import React from "react";
import { getCharacters, ICharacter, IInfo, IFilter } from "./services/http";
import List from "./components/list/List";
import "./App.scss";

function App() {
  const getList = (
    pageNo: number,
    searchExpr?: string,
    filter?: IFilter
  ): Promise<{ info: IInfo; items: ICharacter[] }> => {
    return new Promise((resolve, reject) => {
      // send request with parameters
      getCharacters(pageNo, searchExpr, filter)
        .then((response) => {
          resolve({ info: response.data.info, items: response.data.results });
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  // the List component expects a function that returns a promise with the items to be rendered in the list
  return (
    <div className="app-container">
      <h1>Rick and Morty</h1>
      <h3>Character List</h3>
      <List getList={getList} />
    </div>
  );
}

export default App;
