import React from "react";
import Enzyme, { shallow, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
Enzyme.configure({ adapter: new Adapter() });
import Filter from "../components/list/filter/Filter";

describe("Filter Component", () => {
  let props;
  let wrapper;

  let spy;

  beforeAll(() => {
    props = {
      filter: { status: "dead" },
      onFilterChange: jest.fn(),
    };

    wrapper = shallow(<Filter {...props} />);
  });

  it("renders Filter component", () => {
    expect(wrapper).not.toBeNull();
  });

  it("renders filter options", () => {
    expect(wrapper.find("input[type='radio']")).toHaveLength(4);
  });

  it("sets option to selected based on state", () => {
    expect(wrapper.find("input[value='dead']").prop("checked")).toBe(true);
  });

  it("calls onFilterChange() when filter value changes", () => {
    wrapper.find("input[value='alive']").simulate("change");
    expect(props.onFilterChange).toHaveBeenCalled();
  });
});
