import React, { useState } from "react";
import "./ListItem.scss";

interface IListItemProps {
  renderHeader: () => JSX.Element;
  renderContent: () => JSX.Element;
}

const ListItem = (props: IListItemProps) => {
  const [open, setOpen] = useState(false);

  const handleToggle = () => {
    setOpen(!open);
  };

  return (
    <div className="list-item-container">
      <div className="list-item-header">
        {props.renderHeader()}
        <button onClick={handleToggle}>{open ? "less" : "more"}</button>
      </div>
      {open ? (
        <div className="list-item-content">{props.renderContent()}</div>
      ) : null}
    </div>
  );
};

export default ListItem;
