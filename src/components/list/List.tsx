import React, { useState, useEffect } from "react";
import { ICharacter, IInfo, IFilter } from "../../services/http";
import ListItem from "./list-item/Listitem";
import Loader from "./loader/Loader";
import Pagination from "./paginaton/Pagination";
import Search from "./search/Search";
import Filter from "./filter/Filter";
import "./List.scss";

interface IListProps {
  getList: (
    pageNo: number,
    searchExpr?: string,
    filter?: IFilter
  ) => Promise<{ info: IInfo; items: ICharacter[] }>;
}

const List = (props: IListProps) => {
  const [list, setList] = useState<ICharacter[]>([]);
  const [pagInfo, setPagInfo] = useState<IInfo>({
    count: 0,
    pages: 0,
  });
  const [currentPage, setCurrentPage] = useState(1);
  const [searchExpr, setSearchExpr] = useState("");
  const [filter, setFilter] = useState({
    status: "",
  });
  const [loading, setLoading] = useState<boolean>(false);

  // did mount: fetch first page of items from API
  useEffect(() => {
    getList(currentPage);
  }, []);

  // fetch list of items whenever paging, seaching or filtering happens
  useEffect(() => {
    getList(currentPage, searchExpr, filter);
  }, [currentPage, searchExpr, filter]);

  const getList = (
    pageNo: number,
    searchExpr?: string,
    filter?: IFilter
  ): void => {
    const { getList } = props;
    setLoading(true);
    // send request
    getList(pageNo, searchExpr, filter)
      .then((response) => {
        const info = response.info;
        const items = response.items;
        const preloaders: Promise<void>[] = [];
        // preloading images
        items.forEach((item: ICharacter) => {
          preloaders.push(
            new Promise<void>((resolve, reject) => {
              const img = new Image();
              img.src = item.image;
              img.onload = () => {
                resolve();
              };
              img.onerror = () => {
                resolve();
              };
            })
          );
        });
        // show list only after all images have been loaded
        Promise.all(preloaders).then(() => {
          setLoading(false);
          setPagInfo(info);
          setList(items);
        });
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
        setPagInfo({
          count: 0,
          pages: 0,
        });
        setList([]);
      });
  };

  const onPageClick = (pageNo: number): void => {
    setCurrentPage(pageNo);
  };

  const onSearchChange = (searchString: string): void => {
    setSearchExpr(encodeURI(searchString));
    setCurrentPage(1);
  };

  const onFilterChange = (status: string): void => {
    setFilter({ status });
    setCurrentPage(1);
  };

  return (
    <div className="list-container">
      {loading ? <Loader /> : null}
      <div className="list-controls-container">
        <Search onSearchChange={onSearchChange} />
        <Filter filter={filter} onFilterChange={onFilterChange} />
        <Pagination
          info={pagInfo}
          currentPage={currentPage}
          onPageClick={onPageClick}
        />
      </div>
      <div className="list-items-container">
        {list.length > 1 ? (
          list.map((character) => (
            // using render props for reusability
            <ListItem
              key={character.id}
              renderHeader={() => (
                <>
                  <img
                    src={character.image}
                    alt={character.name}
                    className="character-image"
                  />
                  <div className="character-name">{character.name}</div>
                </>
              )}
              renderContent={() => (
                <table>
                  <tr>
                    <th>Status:</th>
                    <td>{character.status}</td>
                  </tr>
                  <tr>
                    <th>Species:</th>
                    <td>{character.species}</td>
                  </tr>
                  <tr>
                    <th>Type:</th>
                    <td>{character.type}</td>
                  </tr>
                  <tr>
                    <th>Gender:</th>
                    <td>{character.gender}</td>
                  </tr>
                  <tr>
                    <th>Number of episodes:</th>
                    <td>{character.episode.length}</td>
                  </tr>
                </table>
              )}
            />
          ))
        ) : (
          <p>Nothing found</p>
        )}
      </div>
    </div>
  );
};

export default List;
