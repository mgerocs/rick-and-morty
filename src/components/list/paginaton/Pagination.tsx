import React from "react";
import { IInfo } from "../../../services/http";
import "./Pagination.scss";

interface IPaginationProps {
  info: IInfo;
  currentPage: number;
  onPageClick: (pageNo: number) => void;
}

const Pagination = (props: IPaginationProps) => {
  const createPages = () => {
    const { info } = props;
    const pages = [];
    for (let i = 1; i <= info.pages; i++) {
      pages.push(i);
    }
    return pages;
  };

  const handleClick = (pageNo: number): void => {
    const { onPageClick } = props;
    onPageClick(pageNo);
  };

  const { info, currentPage } = props;
  return (
    <div className="pagination-container">
      {createPages().map((pageNo) => (
        <button
          key={pageNo}
          onClick={() => handleClick(pageNo)}
          className={pageNo === currentPage ? "active" : ""}
        >
          {pageNo}
        </button>
      ))}
    </div>
  );
};

export default Pagination;
