import React from "react";
import "./Filter.scss";

interface IFilterProps {
  filter: { status: string };
  onFilterChange: (status: string) => void;
}

const Filter = (props: IFilterProps) => {
  const { onFilterChange } = props;
  const { status } = props.filter;
  return (
    <fieldset className="status-filter">
      <legend>Status</legend>
      <label htmlFor="filter-status-all">
        <input
          id="filter-status-all"
          type="radio"
          name="status"
          value=""
          checked={status === ""}
          onChange={() => onFilterChange("")}
        />
        All
      </label>
      <label htmlFor="filter-status-alive">
        <input
          id="filter-status-alive"
          type="radio"
          name="status"
          value="alive"
          checked={status === "alive"}
          onChange={() => onFilterChange("alive")}
        />
        Alive
      </label>
      <label htmlFor="filter-status-dead">
        <input
          id="filter-status-dead"
          type="radio"
          name="status"
          value="dead"
          checked={status === "dead"}
          onChange={() => onFilterChange("dead")}
        />
        Dead
      </label>
      <label htmlFor="filter-status-unknown">
        <input
          id="filter-status-unknown"
          type="radio"
          name="status"
          value="unknown"
          checked={status === "unknown"}
          onChange={() => onFilterChange("unknown")}
        />
        Unknown
      </label>
    </fieldset>
  );
};

export default Filter;
