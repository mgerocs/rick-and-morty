import React, { useState, useEffect } from "react";
import "./Search.scss";

interface ISearchProps {
  onSearchChange: (searchExpr: string) => void;
}

const Search = (props: ISearchProps) => {
  const [searchExpr, setSearchExpr] = useState("");

  useEffect(() => {
    // debouncing
    // we do not want to flood the API with requests
    // it waits 500 msec after the last input and sends a single request
    const handler = setTimeout(() => {
      const { onSearchChange } = props;
      onSearchChange(searchExpr);
    }, 500);
    return () => {
      clearTimeout(handler);
    };
  }, [searchExpr]);

  const handleKeyUp = (event: React.KeyboardEvent) => {
    setSearchExpr((event.target as HTMLInputElement).value);
  };

  return (
    <fieldset>
      <legend>Character Name</legend>
      <input
        type="text"
        placeholder="Name"
        onKeyUp={handleKeyUp}
        className="search-input"
      />
    </fieldset>
  );
};

export default Search;
